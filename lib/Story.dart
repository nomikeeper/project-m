import 'dart:async';

import "package:flutter/material.dart";

const edgeInsets_10_all =  const EdgeInsets.all(10.0);
const color_red = const Color(0xFFEA3434);
const primary_color = const Color(0xFFF52549); // Hot pink #F52549
const secondary_color = const Color(0xFFCB0000); // Strawberry #CB0000

class Story extends StatefulWidget{
  @override
  createState() => new StoryViewState();
}

class StoryViewState extends State<Story>{
  @override
  Widget build(BuildContext context){
    return new Container(
      child: new Center(
        child: new Container(
          padding: edgeInsets_10_all,
          child: new Card(
              child: new Container(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    // Title
                    new Container(
                      padding: const EdgeInsets.all( 10.0),
                      decoration: const BoxDecoration(
                        border: const Border(
                          bottom: const BorderSide(width: 1.0, color: primary_color),
                        ),
                      ),
                      child: new Text(
                        "Dawn of a day in the Summer", 
                        style: new TextStyle(
                          fontSize: 24.0, 
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Raleway'
                          )
                      ),
                    ),
                    // Image
                    new Expanded(
                      child: new FittedBox(
                        fit: BoxFit.contain,
                        child: const FlutterLogo(),
                      ),
                    ),
                    // Description
                    new Container(
                      padding: edgeInsets_10_all,
                      child: new Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mauris quis magna feugiat dapibus. Cras tempus dolor quis felis posuere, at aliquam lectus interdum. Donec sollicitudin rutrum malesuada. Ut imperdiet enim placerat aliquam suscipit. Aliquam sed vestibulum lorem, id vehicula lorem. ",
                        style: new TextStyle(
                          fontFamily: "Raleway"
                        )
                      )
                    ),
                    // Category
                    new Container(
                      padding: const EdgeInsets.all(10.0),
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            child: new Column(
                              children: <Widget>[
                                new Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new  Row(
                                    children: <Widget>[
                                      new Text("Sections:", style: new TextStyle(fontFamily: "Raleway", fontWeight: FontWeight.bold),),
                                      new Text("5", style: new TextStyle(fontFamily: "Raleway"),),
                                    ],
                                  )
                                ),
                                new Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("Category:", style: new TextStyle(fontFamily: "Raleway", fontWeight: FontWeight.bold),),
                                      new Flexible(
                                        child: new Text("Entertainment, Fun", overflow: TextOverflow.fade, style: new TextStyle(fontFamily: "Raleway"),),
                                      )
                                    ],
                                  )
                                ),
                                new Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: new Row(
                                    children: <Widget>[
                                      new Icon(Icons.thumb_up),
                                      new Text("40", style: new TextStyle(fontFamily: "Raleway"),)
                                    ],
                                  )
                                ),
                              ],
                            ),
                          ),
                          new Expanded(
                            child: new Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Container(
                                    child: new CircleAvatar(
                                      backgroundColor: Colors.greenAccent,
                                      child: new Image.network("https://img00.deviantart.net/4f43/i/2012/314/f/e/trivium___logo__1_by_lightsinaugust-d5kl5o1.png",
                                                                fit: BoxFit.fitWidth,
                                                                scale: 1.0,),
                                    ),
                                  ),
                                  new Text("Username"),
                                ],
                              )
                          ),
                        ],
                      )
                    ),
                    new Container(
                      decoration: const BoxDecoration(
                        border: const Border(
                        )
                      ),
                      child:  new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                              decoration: const BoxDecoration(
                                color: secondary_color,
                                border: const Border(
                                  right: const BorderSide(width: 1.0, color: const Color(0xFFFFFFFF))
                                )
                              ),
                              padding: const EdgeInsets.symmetric(vertical: 10.0),
                              child: 
                              new FlatButton(
                                  onPressed: (){_neverSatisfied();} ,
                                  
                                  child: new Text("Skip", style: new TextStyle( color: Colors.white, fontFamily: "Raleway")),
                              ),
                            ),
                          ),
                          new Expanded(
                            child:
                              new Container(
                                padding: const EdgeInsets.symmetric(vertical: 10.0),
                                color: primary_color,
                                child: new FlatButton(
                                  onPressed: (){ Navigator.of(context).pushNamed('/FullStory');} ,
                                  child: new Text("Read", style: new TextStyle( color: Colors.white, fontFamily: "Raleway"))
                                ),
                              )
                          ),
                        ],
                      )
                      
                    )
                  ],
                )
              )
          )
        ),
      ),
    );
  }

  Future<Null> _neverSatisfied() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      child: new AlertDialog(
        title: new Text('Rewind and remember'),
        content: new SingleChildScrollView(
          child: new ListBody(
            children: <Widget>[
              new Text('You will never be satisfied.'),
              new Text('You’re like me. I’m never satisfied.'),
            ],
          ),
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Regret'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}

