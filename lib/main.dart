import 'package:flutter/material.dart';
import "./Story.dart";
import "./Profile.dart";
import "./FullStory.dart";
import "./Archive.dart";
import "./Quiz.dart";
import "./Home/Home.dart";

const primary_color = const Color(0xFFF52549); // Hot pink #F52549

void main() => runApp(new Movies());

class App extends StatefulWidget{
  App({Key key}) : super(key: key);
  @override
  createState() => new AppState();
}

class AppState extends State<App> with SingleTickerProviderStateMixin {

  // Global variable
  int _index = 0;

  final List<Tab> Tabs = <Tab>[
    new Tab(icon: new Icon(Icons.pages),),
    new Tab(icon: new Icon(Icons.person_pin_circle),),
    new Tab(icon: new Icon(Icons.book),),
    new Tab(icon: new Icon(Icons.account_box),),
  ];

  TabController _controller;
  String title = "My Story";

  @override
  void initState(){
    super.initState();
    _controller = new TabController(vsync: this, length: Tabs.length);
    _controller.addListener(handleSelected);
  }

  void handleSelected(){
    setState((){
      title = Tabs[_controller.index].text;
    });
  }
  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new DefaultTabController(
        length: Tabs.length,
        child:  new Scaffold(
          appBar: new AppBar(
            title: new Text(
              title, 
              style: new TextStyle(
                color: Colors.white, 
                fontSize: 24.0, 
                fontStyle: FontStyle.italic, 
                fontWeight: FontWeight.w300
              ),
            ),
            backgroundColor: primary_color,
          ),
          body: new TabBarView(
            controller: _controller,
            children: <Widget>[
              new Story(),
              new Quiz(),
              new Archive(),
              new Profile(),
            ]
          ),
          bottomNavigationBar: new Material(
            color: primary_color,
            child: new TabBar(
              unselectedLabelColor: Colors.white,
              indicatorColor: Colors.white,
              controller: _controller,
              tabs: Tabs,
            ),
          )
        )
      ),
      routes: <String, WidgetBuilder>{
        '/FullStory': (BuildContext context) => new FullStory()
      },
    );
  }
}

class Movies extends StatefulWidget{
  Movies({Key key}): super(key: key);
  @override
  createState() => new MoviesState();
}

class MoviesState extends State<Movies> with SingleTickerProviderStateMixin{

  // Global variables
  int current_index = 0;

  TabController _controller;

  // Initialize the State
  @override
  void initState(){
    super.initState();
    _controller = new TabController(vsync: this, length: Tabs.length);
    _controller.addListener(handleSelected);
  }

  // Handle when Tab bar controller is used
  void handleSelected(){
    setState((){

    });
  }

  // Dispose
  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }

  // Tab Views
  List <Tab> Tabs = <Tab>[
    new Tab(icon: new Icon(Icons.home),),
    new Tab(icon: new Icon(Icons.search),),
    new Tab(icon: new Icon(Icons.menu),)
  ];
  
  // Main build function
  @override
  Widget build(BuildContext context){
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: new DefaultTabController(
        length: Tabs.length,
        child: new Scaffold(
          appBar: new AppBar(
            backgroundColor: Colors.amberAccent,
            title: new Text("Filmmakers!"),
          ),
          body: new TabBarView(
            controller: _controller,
            children: <Widget>[
              new Home(),
              new Archive(),
              new Profile(),
            ],
          ),
          bottomNavigationBar: new Material(
            color: Colors.amberAccent,
            child: new TabBar(
              unselectedLabelColor: Colors.white,
              indicatorColor: Colors.black,
              controller: _controller,
              tabs: Tabs,
            ),
          ),
        ),
      ),
    );
  }
}
