import "package:flutter/material.dart";

// Colors
const secondary_color = const Color(0xFFCB0000); // Strawberry #CB0000


class Archive extends StatefulWidget{
  @override
  createState() => new ArchiveState();
}

class ListItem {
  const ListItem ({this.title,this.author});
  final String title;
  final String author;

}

const List<ListItem> items = const <ListItem>[
  const ListItem(title: "Title 1", author: "Author 1"),
  const ListItem(title: "Title 2", author: "Author 2"),
  const ListItem(title: "Title 3", author: "Author 3"),
  const ListItem(title: "Title 4", author: "Author 4"),
  const ListItem(title: "Title 5", author: "Author 5"),
  const ListItem(title: "Title 1", author: "Author 1"),
  const ListItem(title: "Title 2", author: "Author 2"),
  const ListItem(title: "Title 3", author: "Author 3"),
  const ListItem(title: "Title 4", author: "Author 4"),
  const ListItem(title: "Title 5", author: "Author 5"),
  const ListItem(title: "Title 1", author: "Author 1"),
  const ListItem(title: "Title 2", author: "Author 2"),
  const ListItem(title: "Title 3", author: "Author 3"),
  const ListItem(title: "Title 4", author: "Author 4"),
  const ListItem(title: "Title 5", author: "Author 5"),
  const ListItem(title: "Title 1", author: "Author 1"),
  const ListItem(title: "Title 2", author: "Author 2"),
  const ListItem(title: "Title 3", author: "Author 3"),
  const ListItem(title: "Title 4", author: "Author 4"),
  const ListItem(title: "Title 5", author: "Author 5"),
  const ListItem(title: "Title 1", author: "Author 1"),
  const ListItem(title: "Title 2", author: "Author 2"),
  const ListItem(title: "Title 3", author: "Author 3"),
  const ListItem(title: "Title 4", author: "Author 4"),
  const ListItem(title: "Title 5", author: "Author 5"),
];

class ArchiveState extends State<Archive>{

  TextEditingController _textController = new TextEditingController();

  void _updateList(String value){
    print("Incoming value: " + value);
    _textController.clear();
  }

  void _onChange(String value){
    print("Incoming value: " + value);
  }


  void _openStory(){
    print("Opening Story");
    Navigator.of(context).pushNamed('/FullStory');
  }

  @override
  Widget build(BuildContext context){

    return new Container(
      child: new Column(
        children: <Widget>[
          new Expanded(
            flex:1,
            child: new ListView.builder(
              itemCount: items.length,
              itemBuilder: (context, index) {
                return new Container(
                  decoration: const BoxDecoration(
                    border: const Border(
                      bottom: const BorderSide(width: 1.0, color: secondary_color)
                    )
                  ),
                  child: new ListTile(
                    leading: new Icon(Icons.camera),
                    title: new Text(items[index].title),
                    onTap: _openStory,
                    trailing: new Text(
                      items[index].author
                    ),
                  )
                );
              },
            )
          ),
        ]
      )
    );
  }
}