import 'package:flutter/material.dart';

// # Colors
const primary_color = const Color(0xFFF52549); // Hot pink #F52549
const secondary_color = const Color(0xFFCB0000); // Strawberry #CB0000

class FullStory extends StatefulWidget{
  @override
  createState() => new FullStoryState();
}

class Action {
  const Action({ this.title, this.icon });
  final String title;
  final IconData icon;
}

const List<Action> actions = const <Action>[
  const Action(title: 'Save', icon: Icons.favorite),
  const Action(title: 'Like', icon: Icons.thumb_up),
];

class FullStoryState extends State<FullStory>{

  List sections = [
    new Container(
      child: new Column(
        children: <Widget>[
          new Text("Subtitle 1", style: new TextStyle(fontFamily: "Raleway", fontStyle: FontStyle.italic)),
          new Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mauris quis magna feugiat dapibus. Cras tempus dolor quis felis posuere, at aliquam lectus interdum. Donec sollicitudin rutrum malesuada. Ut imperdiet enim placerat aliquam suscipit. Aliquam sed vestibulum lorem, id vehicula lorem. ",
            style: new TextStyle(
              fontFamily: "Raleway",
              fontSize: 18.0
            )
          ),
          new Divider(height: 40.0, color: primary_color,),
        ],
      ),
    ),
    new Container(
      child: new Column(
        children: <Widget>[
          new Text("Subtitle 2", style: new TextStyle(fontFamily: "Raleway", fontStyle: FontStyle.italic)),
          new Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mauris quis magna feugiat dapibus. Cras tempus dolor quis felis posuere, at aliquam lectus interdum. Donec sollicitudin rutrum malesuada. Ut imperdiet enim placerat aliquam suscipit. Aliquam sed vestibulum lorem, id vehicula lorem. ",
            style: new TextStyle(
              fontFamily: "Raleway",
              fontSize: 18.0
            )
          ),
          new Divider(height: 40.0, color: primary_color,),
        ],
      ),
    )
  ];

  void _onSelected(String value){
      switch(value){
        case "Save":
          print("Save method!");
          break;
        case "Like":
          print("Like method");
          break;
        default: break;
      }
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: primary_color,
        title: new Text("Story title", style: new TextStyle(fontFamily: "Raleway", fontSize: 24.0),),
        actions: <Widget>[
          new PopupMenuButton( 
            onSelected: _onSelected,
            itemBuilder: (BuildContext context) {
              return actions.map((Action action) {
                return new PopupMenuItem(
                  value: action.title,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Icon(action.icon),
                      new Text(action.title),
                    ],
                  ) 
                );
              }).toList();
            },
          ),
        ],
      ),
      body: new Container(
        padding: const EdgeInsets.all(10.0),
        child: new ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(20.0),
              children: <Widget>[
                // Story image
                new Container(
                  margin: const EdgeInsets.only(bottom: 40.0),
                  child:  new Image.network(
                    'https://raw.githubusercontent.com/flutter/website/master/_includes/code/layout/lakes/images/lake.jpg',
                    fit: BoxFit.fitWidth,
                    scale: 1.0,
                  )
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        margin: const EdgeInsets.only(bottom: 10.0),
                        child: new Text("Subtitle 1", style: new TextStyle(fontFamily: "Raleway", fontStyle: FontStyle.italic, fontSize:20.0, color: const Color(0xFFAAAAAA))),
                      ),
                      new Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mauris quis magna feugiat dapibus. Cras tempus dolor quis felis posuere, at aliquam lectus interdum. Donec sollicitudin rutrum malesuada. Ut imperdiet enim placerat aliquam suscipit. Aliquam sed vestibulum lorem, id vehicula lorem. ",
                        style: new TextStyle(
                          fontFamily: "Raleway",
                          fontSize: 18.0
                        )
                      ),
                      new Divider(height: 40.0, color: const Color(0xFFCCCCCC),),
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        margin: const EdgeInsets.only(bottom: 10.0),
                        child: new Text("Subtitle 2", style: new TextStyle(fontFamily: "Raleway", fontStyle: FontStyle.italic, fontSize:20.0, color: const Color(0xFFAAAAAA)))
                      ),
                      new Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mauris quis magna feugiat dapibus. Cras tempus dolor quis felis posuere, at aliquam lectus interdum. Donec sollicitudin rutrum malesuada. Ut imperdiet enim placerat aliquam suscipit. Aliquam sed vestibulum lorem, id vehicula lorem. ",
                        style: new TextStyle(
                          fontFamily: "Raleway",
                          fontSize: 18.0,
                        )
                      ),
                      new Divider(height: 40.0, color: const Color(0xFFCCCCCC),),
                    ],
                  ),
                ),
              ]
            ),
      ),
    );
  }
}
