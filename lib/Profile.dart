import "package:flutter/material.dart";

// # Colors
const primary_color = const Color(0xFFF52549); // Hot pink #F52549
const secondary_color = const Color(0xFFCB0000); // Strawberry #CB0000

// Margins
const vertical_10 =  const EdgeInsets.symmetric(vertical: 10.0);
const vertical_20 =  const EdgeInsets.symmetric(vertical: 20.0);

const horizontal_30 =  const EdgeInsets.symmetric(horizontal: 30.0);
// Account info styles
const accInfoLabel = const TextStyle(fontFamily: "Raleway", fontSize: 16.0, fontWeight: FontWeight.bold);
const accInfoValue = const TextStyle(fontFamily: "Raleway", fontSize: 16.0, fontStyle: FontStyle.italic);

class Profile extends StatefulWidget{
  @override
  createState() => new ProfileState();
}

class ProfileState extends State<Profile>{

  bool _switchState = false;

  void _switchChanged(bool value){
    setState((){
      _switchState = value;
    });
  }

  @override
  Widget build(BuildContext context){
    return 
      new ListView(
        shrinkWrap: true,
        children: <Widget>[
          new Column(
            children: <Widget>[
              // # Profile picture section
              new Container(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                decoration: const BoxDecoration(
                  border: const Border(
                    bottom: const BorderSide(width:1.0, color: primary_color)
                  ),
                ),
                child: new Center(
                  child: new Column(
                    children: <Widget>[
                      new CircleAvatar(
                          backgroundColor: Colors.deepOrangeAccent,
                          backgroundImage: new NetworkImage("https://www.mobafire.com/images/champion/square/zed.png"),
                          radius: 75.0,
                      ),
                      new Container(
                        padding: const EdgeInsets.symmetric(vertical:10.0),
                        child: new Text("Username", style: new TextStyle(fontSize: 20.0, fontFamily: "Raleway"),)
                      )
                    ],
                  ),
                ),
              ),
              //  # Basic info section
              new Container(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                      margin: const EdgeInsets.symmetric(vertical: 15.0),
                      child: new Text("Account Info", style: new TextStyle(fontSize: 24.0, color: const Color(0xFF626262)))
                    ),
                    new Container(
                      margin: horizontal_30,
                      padding: vertical_10,
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Text("First Name :", style: accInfoLabel,),
                          ),
                          new Expanded(
                            flex:2,
                            child: new Text("Zed", style: accInfoValue,)
                          )
                        ],
                      )
                    ),
                    new Container(
                      margin: horizontal_30,
                      padding: vertical_10,
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Text("Last Name :", style: accInfoLabel,),
                          ),
                          new Expanded(
                            flex:2,
                            child: new Text("Master of Shadow", style: accInfoValue,)
                          )
                        ],
                      )
                    ),
                    new Container(
                      margin: horizontal_30,
                      padding: vertical_10,
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Text("E-mail :", style: accInfoLabel,),
                          ),
                          new Expanded(
                            flex:2,
                            child: new Text("nomikeeper@gmail.com", style: accInfoValue,)
                          )
                        ],
                      )
                    ),
                    new Container(
                      margin: horizontal_30,
                      padding: vertical_10,
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Text("Mobile :", style: accInfoLabel,),
                          ),
                          new Expanded(
                            flex:2,
                            child: new Text("xxx-xxxx-xxxx", style: accInfoValue,)
                          )
                        ],
                      )
                    ),
                    new Container(
                      margin: vertical_20,
                      padding: vertical_20,
                      decoration: const BoxDecoration(
                        border: const Border(
                          top: const BorderSide(width: 1.0, color: const Color(0xFFdedede)),
                          bottom: const BorderSide(width: 1.0, color: const Color(0xFFdedede))
                        )
                      ),
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Column(
                              children: <Widget>[
                                new Icon(Icons.account_balance_wallet),
                                new Text("200dc"),
                              ],
                            ),
                          ),
                          new Expanded(
                            flex: 1,
                            child: new Column(
                              children: <Widget>[
                                new Icon(Icons.file_download),
                                new Text("Deposit"),
                              ],
                            ),
                          ),
                          new Expanded(
                            flex: 1,
                            child: new Column(
                              children: <Widget>[
                                new Icon(Icons.file_upload),
                                new Text("Withdraw"),
                              ],
                            ),
                          ),
                        ], 
                      ),
                    ),
                    new Container(
                      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                      margin: const EdgeInsets.symmetric(vertical: 15.0),
                      child: new Switch(
                        value: _switchState,
                        onChanged: _switchChanged,
                      )
                    ),
                  ],
                ),
              ),
              //  # Subscription section
            ],
          )
        ],
      );
  }
}