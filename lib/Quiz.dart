import "package:flutter/material.dart";

const primary_color = const Color(0xFFF52549); // Hot pink #F52549

class Quiz extends StatefulWidget{

  @override
  createState() => new QuizState();
}

class QuizState extends State<Quiz>{
  @override
  Widget build(BuildContext context){
    return new Container(
      child: new Column(
        children: <Widget>[
          // Top Choice section
          new Expanded(
            flex:4,
            child: new Container(
              color: const Color(0xFF00FCAC),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Text(
                      "Choice 1"
                    ),
                  )
                ],
              ),
            )
          ),
          // Quiz Section
          new Expanded(
            flex:1,
            child: new Container(
              color: primary_color,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                      "Quiz", 
                      style: new TextStyle(
                        fontFamily: "Raleway",
                        color: const Color(0xFFFFFFFF)
                    ),
                  ),
                ],
              )
            )
          ),
          new Expanded(
            flex: 4,
            child: new Container(
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Text(
                      "Choice 2"
                    ),
                  )
                ],
              ),
            )
          ),
        ],
      ),
    );
  }
}