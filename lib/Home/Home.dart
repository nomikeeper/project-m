import 'package:flutter/material.dart';

class Home extends StatefulWidget{
  @override
  createState() => new HomeState();
}

class HomeState extends State<Home>{
  @override
  Widget build(BuildContext context){
    return new Container(
      color: Colors.black,
      child: new Column(
        children: <Widget>[
          new FeaturedMovie(),
          new Container(
            margin: const EdgeInsets.only(top:40.0),
            child: new Text("Coming soon", style: new TextStyle(fontSize: 26.0, fontFamily: "Raleway", fontWeight: FontWeight.w300, color: Colors.white, fontStyle: FontStyle.italic),),
          )
        ],
      ),
    );
  }
}

class FeaturedMovie extends StatelessWidget{

  @override
  Widget build(BuildContext context){
    double _scWidth = MediaQuery.of(context).size.width;
    double _featuredMovie = 300.0;
    return new Container(
      child: new Column(
        children: <Widget>[
          // Background
          new Container(
            color: Colors.black,
            width: _scWidth,
            height: _featuredMovie,
            child: new FittedBox(
              fit: BoxFit.contain,
              child: new Image.asset(
                "resource/img/venom.jpg",
                fit: BoxFit.contain,
              ),
            ), 
          ),
          // Bottom footer
          new Container(
            child: new Container(
              decoration: const BoxDecoration(
              border: const Border(
                top: const BorderSide( color:Colors.amberAccent, width:1.0,)
              ),
              color: Colors.black87,
            ),
              padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              width: _scWidth,
              child: new Row(
                children: <Widget>[
                  new Text("Name: ", style: new TextStyle(color: Colors.white),), 
                  new Text("Venom", style: new TextStyle(color: Colors.grey, fontSize: 20.0, fontFamily: "Raleway"),)
                ],
              ) 
            ),
          ),
          new Container(
            child: new Container(
              padding: const EdgeInsets.fromLTRB(15.0,0.0,15.0,10.0),
              width: _scWidth,
              color: Colors.black87,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    flex:2,
                    child: new Container(
                      child:  new Text("Starring: ", style: new TextStyle(color: Colors.white),),
                    ) 
                  ),
                  new Flexible(
                    flex:8,
                    child: new Container(
                      child: new Text("Tom Hardy, Michelle Williams, Tom Holland...", style: new TextStyle(color: Colors.grey, fontStyle: FontStyle.italic, fontFamily: "Raleway"),),
                    )
                  )
                ],
              ) 
            ),
          ),
          new Container(
            decoration: const BoxDecoration(
              border: const Border(
                bottom: const BorderSide( color:Colors.amberAccent, width:1.0,)
              ),
              color: Colors.black,
            ),
            width: _scWidth,
            child: new Center(
              // Button wrapper
              child: new Container(
                decoration: const BoxDecoration(
                    border: const Border(
                      top: const BorderSide(color: Colors.amberAccent,width: 1.0,),
                      bottom: const BorderSide(color: Colors.amberAccent,width: 1.0,),
                      left: const BorderSide(color: Colors.amberAccent,width: 1.0,),
                      right: const BorderSide(color: Colors.amberAccent,width: 1.0,),
                    ),
                ),
                margin: const EdgeInsets.symmetric(vertical: 10.0),
                // Raised Button
                child: new RaisedButton(
                    onPressed: (){ 
                        var alert = new AlertDialog(
                          title: new Text("View more..."),
                          content: new Text("You are about to read more about this movie"),
                        );
                        showDialog(context: context, child: alert);
                        
                    },
                    child: new Text("Read"),
                    splashColor: Colors.amberAccent,
                    color: Colors.black,
                    textColor: Colors.amberAccent,
                    highlightColor: Colors.white,
                    
                  )
              ) 
            ) 
            /*
              new IconButton(
                onPressed: (){ 
                  var alert = new AlertDialog(
                    title: new Text("View more..."),
                    content: new Text("You are about to read more about this movie"),
                  );
                  showDialog(context: context, child: alert);
                },
                color: Colors.amberAccent,
                icon: new Icon(Icons.play_circle_outline),
                iconSize: 40.0,
              ),
            */
          )
        ],
      ),
    );
  }
}